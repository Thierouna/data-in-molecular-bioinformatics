import java.io.IOException;
import java.util.*;

public class Main {

   /* public static Set<CommonElement> getCommonOfNMRDistance(List<Atom> h, List<NMRDistance> NMR) {
        Set<CommonElement> distances = new LinkedHashSet<>();
        HashMap<String, Double> distanceMap = new HashMap<>();
        for(NMRDistance nmr : NMR) {
            OutputForm opf = new OutputForm(nmr.atomName1, nmr.atomName2, nmr.seqID1, nmr.seqID2);
            for (int j = 0; j < h.size(); j++)
                if (j + 1 < h.size()) {
                    if ((nmr.atomName1.equals(h.get(j).getName()) && (nmr.atomName2.equals(h.get(j + 1).getName())))
                            && (nmr.seqID1 == h.get(j).getSeqID() && nmr.seqID2 == h.get(j + 1).getSeqID())) {
                        double distance = calculateEuclideanDistance(h.get(j).getX(), h.get(j+1).getX(), h.get(j).getY(),h.get(j+1).getY(), h.get(j).getZ(), h.get(j+1).getZ());
                        String key = h.get(j).getName() + "\n" + h.get(j + 1).getName() + "\n" + h.get(j).getSeqID() + "\n" + h.get(j + 1).getSeqID();
                        if (!distanceMap.containsKey(key)) {
                            distanceMap.put(key, distance);
                            distances.add(new CommonElement(h.get(j).getName(), h.get(j + 1).getName(), h.get(j).getSeqID(), h.get(j + 1).getSeqID(), nmr.minDistance, nmr.maxDistance));
                        }
                    }
                }
        }
        return distances;
    }
*/
    public static void main(String[] args) throws IOException {
        Fonctions fct = new Fonctions();
        NMR str, nmr, nmr_1, nmr_2;
        str = new NMR("C:\\Users\\souma\\OneDrive\\Bureau\\NMR\\2jmy_mr.str");
        nmr = new NMR("C:\\Users\\souma\\OneDrive\\Bureau\\NMR\\2jmy.mr");
        List<Atom> atomList;
        atomList = fct.extractHydrogens("C:\\Users\\souma\\rmsd\\proteins\\2jod\\2jod_pred_model_5.pdb");
        List<NMRDistance> strList = str.getNMRdistances();
        List<NMRDistance> nmrList = nmr.getNMRdistances();
        GetFilesElement strDistances = fct.constructAllPossibleAtomCoupleOfPdbFileFromStrFile(atomList, strList);
        GetFilesElement nrmDistances = fct.constructAllPossibleAtomCoupleOfPdbFileFromNmrFile(atomList, nmrList);
        Set<OutputForm> compare = fct.compareDistancesOfAllAtomCoupleOfPdbFileAndStrOrNmrFile(strList, strDistances.getOutputFormList());
        Set<OutputForm> nrmCompare = fct.compareDistancesOfAllAtomCoupleOfPdbFileAndStrOrNmrFile(nmrList, nrmDistances.getOutputFormList());
        double strDistanceError = fct.computationDistanceError(strDistances.getCommonElementList(), strDistances.getOutputFormList());
        double nmrDistanceError = fct.computationDistanceError(nrmDistances.getCommonElementList(), nrmDistances.getOutputFormList());
        Iterator<OutputForm> it = strDistances.getOutputFormList().iterator();
        /*Iterator<CommonElement> it1 = strDistances.getCommonElementList().iterator();
        while (it1.hasNext() && it.hasNext()) {
            CommonElement commonElement = it1.next();
            OutputForm outputForm = it.next();

            System.out.println(commonElement.getAtomName1() + " " + commonElement.getAtomName2() + " " +
                    commonElement.getSeqID1() + " " + commonElement.getSeqID2() + " " + commonElement.getDistanceMin() +
                    " " + commonElement.getDistanceMax() + " <---> " + outputForm.getAtomName1() + " " +
                    outputForm.getAtomName2() + " " + outputForm.getSeqID1() + " " + outputForm.getSeqID2() + " " +
                    outputForm.getDistanceVal());
        }
        for (OutputForm out : compare) {
            System.out.println(out.getAtomName1() + " " +
                    out.getAtomName2() + " " + out.getSeqID1() + " " + out.getSeqID2() + " " +
                    out.getDistanceVal());
        }
    }*/
        }
}