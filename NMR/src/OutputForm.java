public class OutputForm {
    private String atomName1, atomName2;
    private int seqID1, seqID2;
    private double distanceVal;

    public OutputForm(String atomName1, String atomName2, int seqID1, int seqID2, double distanceVal) {
        this.atomName1 = atomName1;
        this.atomName2 = atomName2;
        this.seqID1 = seqID1;
        this.seqID2 = seqID2;
        this.distanceVal = distanceVal;
    }
    public OutputForm(String atomName1, String atomName2, int seqID1, int seqID2){
        this.atomName1 = atomName1;
        this.atomName2 = atomName2;
        this.seqID1 = seqID1;
        this.seqID2 = seqID2;
    }
    public String getAtomName1() {
        return atomName1;
    }

    public void setAtomName1(String atomName1) {
        this.atomName1 = atomName1;
    }

    public String getAtomName2() {
        return atomName2;
    }

    public void setAtomName2(String atomName2) {
        this.atomName2 = atomName2;
    }

    public int getSeqID1() {
        return seqID1;
    }

    public void setSeqID1(int seqID1) {
        this.seqID1 = seqID1;
    }

    public int getSeqID2() {
        return seqID2;
    }

    public void setSeqID2(int seqID2) {
        this.seqID2 = seqID2;
    }

    public double getDistanceVal() {
        return distanceVal;
    }

    public void setDistanceVal(double distanceVal) {
        this.distanceVal = distanceVal;
    }
}
