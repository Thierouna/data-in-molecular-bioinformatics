import java.util.Set;

public class GetFilesElement {
    private Set<CommonElement> commonElementList;
    private Set<OutputForm> outputFormList;

    public GetFilesElement(Set<CommonElement> commonElementList, Set<OutputForm> outputFormList) {
        this.commonElementList = commonElementList;
        this.outputFormList = outputFormList;
    }

    public GetFilesElement() {
    }

    public Set<CommonElement> getCommonElementList() {
        return commonElementList;
    }

    public void setCommonElementList(Set<CommonElement> commonElementList) {
        this.commonElementList = commonElementList;
    }

    public Set<OutputForm> getOutputFormList() {
        return outputFormList;
    }

    public void setOutputFormList(Set<OutputForm> outputFormList) {
        this.outputFormList = outputFormList;
    }
}
