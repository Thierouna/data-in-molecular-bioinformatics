import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Fonctions {
    public List<Atom> extractHydrogens(String filename) throws IOException {
        // Create a list to store the hydrogen atoms
        List<Atom> hydrogens = new ArrayList<>();
        // Create a buffered reader to read the PDB file
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        // Read the lines of the PDB file
        String line;
        while ((line = reader.readLine()) != null) {
            // Check if the line starts with "ATOM"
            if (line.startsWith("ATOM")) {
                // Extract the atom name
                String atomName = line.substring(12, 16).trim();
                // Check if the atom name starts with "H"
                if (atomName.startsWith("H")) {
                    // Extract the x, y, and z coordinates of the atom
                    double x = Double.parseDouble(line.substring(30, 38));
                    double y = Double.parseDouble(line.substring(38, 46));
                    double z = Double.parseDouble(line.substring(46, 54));
                    // Extract the sequence ID of the residue to which the atom belongs
                    int sequenceID = Integer.parseInt(line.substring(23, 26).trim());
                    // Create a new Atom object and add it to the list
                    hydrogens.add(new Atom(atomName, sequenceID,x, y, z));
                }
            }
        }
        // Close the buffered reader
        reader.close();
        // Return the list of hydrogen atoms
        return hydrogens;
    }
    public GetFilesElement constructAllPossibleAtomCoupleOfPdbFileFromStrFile(List<Atom> h, List<NMRDistance> NMR) {
        GetFilesElement distances = new GetFilesElement();
        HashMap<String, Double> distanceMap = new HashMap<>();
        Set<CommonElement> list1 = new LinkedHashSet<>();
        Set<OutputForm> list2 = new LinkedHashSet<>();
        return getGetFilesElement(h, NMR, distanceMap, list1, list2);
    }
    public GetFilesElement constructAllPossibleAtomCoupleOfPdbFileFromNmrFile(List<Atom> h, List<NMRDistance> NMR){
        List<NMRDistance> newList = new ArrayList<>();
        HashMap<String, Double> distanceMap = new HashMap<>();
        Set<CommonElement> list1 = new LinkedHashSet<>();
        Set<OutputForm> list2 = new LinkedHashSet<>();
        GetFilesElement distances = new GetFilesElement();
        for (NMRDistance item : NMR) {
            for(int j=1; j<10; j++) {
                String atom1 = item.atomName1.replace("#", Integer.toString(j));
                for(int n=1; n<10; n++){
                    newList.add(new NMRDistance(
                            atom1,
                            item.atomName2.replace("#", Integer.toString(n)),
                            item.seqID1,
                            item.seqID2,
                            item.minDistance,
                            item.maxDistance,
                            item.distanceVal));
                }

            }
        }
        return getGetFilesElement(h, newList, distanceMap, list1, list2);
    }

    private GetFilesElement getGetFilesElement(List<Atom> h, List<NMRDistance> newList, HashMap<String, Double> distanceMap, Set<CommonElement> list1, Set<OutputForm> list2) {
        GetFilesElement distances;
        for(NMRDistance nmr : newList) {
            OutputForm opf = new OutputForm(nmr.atomName1, nmr.atomName2, nmr.seqID1, nmr.seqID2);
            for (int j = 0; j < h.size(); j++)
                if (j + 1 < h.size()) {
                    if ((nmr.atomName1.equals(h.get(j).getName()) && (nmr.atomName2.equals(h.get(j + 1).getName())))
                            && (nmr.seqID1 == h.get(j).getSeqID() && nmr.seqID2 == h.get(j + 1).getSeqID())) {
                        double distance = this.computeEuclideanDistance(h.get(j).getX(), h.get(j+1).getX(),
                                h.get(j).getY(),h.get(j+1).getY(), h.get(j).getZ(), h.get(j+1).getZ());
                        String key = h.get(j).getName() + "\n" + h.get(j + 1).getName() + "\n" + h.get(j).getSeqID() +
                                "\n" + h.get(j + 1).getSeqID();
                        if (!distanceMap.containsKey(key)) {
                            distanceMap.put(key, distance);
                            list1.add(new CommonElement(nmr.atomName1, nmr.atomName2, nmr.seqID1,
                                    nmr.seqID2, nmr.minDistance, nmr.maxDistance));
                            list2.add(new OutputForm(h.get(j).getName(), h.get(j + 1).getName(),
                                    h.get(j).getSeqID(), h.get(j + 1).getSeqID(), distance));
                        }
                    }
                }
        }
        return distances = new GetFilesElement(list1, list2);
    }

    public Set<OutputForm> compareDistancesOfAllAtomCoupleOfPdbFileAndStrOrNmrFile(List<NMRDistance> NMR,
                                                                                  Set<OutputForm> OPF) {
        Set<OutputForm> element = new LinkedHashSet<>();
        HashMap<String, Double> distanceMap = new HashMap<>();
        for (NMRDistance nmr : NMR) {
            for (OutputForm opf : OPF) {
                if ((nmr.atomName1.equals(opf.getAtomName1()) && nmr.atomName2.equals(opf.getAtomName2()))
                        && (nmr.seqID1 == opf.getSeqID1() && nmr.seqID2 == opf.getSeqID2())) {
                    if (opf.getDistanceVal() > nmr.minDistance && opf.getDistanceVal() < nmr.maxDistance) {
                        String key = opf.getAtomName1()+"\n"+opf.getAtomName2()+"\n"+opf.getSeqID1()+"\n"+
                                opf.getSeqID2();
                        if (!distanceMap.containsKey(key)) {
                            distanceMap.put(key, opf.getDistanceVal());
                            element.add(new OutputForm(opf.getAtomName1(), opf.getAtomName2(), opf.getSeqID1(),
                                    opf.getSeqID2(), opf.getDistanceVal()));
                        }
                    }
                }
            }
        }

        return element;
    }
    public  double computationDistanceError(Set<CommonElement> cm, Set<OutputForm> set){
        Iterator<CommonElement> it1 = cm.iterator();
        Iterator<OutputForm> it2 = set.iterator();
        double error = 0.0;
        int size = 0;
        while(it1.hasNext() && it2.hasNext()) {
            CommonElement cm1 = it1.next();
            OutputForm of = it2.next();
            if ((of.getDistanceVal() < cm1.getDistanceMin()) || (of.getDistanceVal() > cm1.getDistanceMax())){
                error += Math.abs(Math.min(of.getDistanceVal() - cm1.getDistanceMin(),
                        cm1.getDistanceMax() - of.getDistanceVal()));
                ++size;
            }
        }
        return (double) error / size;
    }
    public  double computeEuclideanDistance(double x1, double x2, double y1, double y2, double z1, double z2){
        return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2) + Math.pow(z1 - z2, 2));
    }
}
