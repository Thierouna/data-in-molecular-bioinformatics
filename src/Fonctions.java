import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Fonctions {
    public List<Atom> extractHydrogens(String filename) throws IOException {
        List<Atom> hydrogens = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        String line;
        while ((line = reader.readLine()) != null) {
            if (line.startsWith("ATOM")) {
                String atomName = line.substring(12, 16).trim();
                if (atomName.startsWith("H")) {
                    double x = Double.parseDouble(line.substring(30, 38));
                    double y = Double.parseDouble(line.substring(38, 46));
                    double z = Double.parseDouble(line.substring(46, 54));
                    int sequenceID = Integer.parseInt(line.substring(23, 26).trim());
                    hydrogens.add(new Atom(atomName, sequenceID,x, y, z));
                }
            }
        }
        return hydrogens;
    }
    public GetFilesElement constructAllPossibleAtomCoupleOfPdbFileFromStrOrMrFile(List<Atom> h, List<NMRDistance> NMR) {
        GetFilesElement distances = new GetFilesElement();
        HashMap<String, Double> distanceMap = new HashMap<>();
        Set<CommonElement> list1 = new LinkedHashSet<>();
        Set<OutputForm> list2 = new LinkedHashSet<>();
        for(NMRDistance nmr : NMR) {
            OutputForm opf = new OutputForm(nmr.atomName1, nmr.atomName2, nmr.seqID1, nmr.seqID2);
            for (int j = 0; j < h.size(); j++)
                if (j + 1 < h.size()) {
                    if ((nmr.atomName1.equals(h.get(j).getName()) && (nmr.atomName2.equals(h.get(j + 1).getName())))
                            && (nmr.seqID1 == h.get(j).getSeqID() && nmr.seqID2 == h.get(j + 1).getSeqID())) {
                        double distance = this.calculateEuclideanDistance(h.get(j).getX(), h.get(j+1).getX(), h.get(j).getY(),h.get(j+1).getY(), h.get(j).getZ(), h.get(j+1).getZ());
                        String key = h.get(j).getName() + "\n" + h.get(j + 1).getName() + "\n" + h.get(j).getSeqID() + "\n" + h.get(j + 1).getSeqID();
                        if (!distanceMap.containsKey(key)) {
                            distanceMap.put(key, distance);
                            list1.add(new CommonElement(nmr.atomName1, nmr.atomName2, nmr.seqID1, nmr.seqID2, nmr.minDistance, nmr.maxDistance));
                            list2.add(new OutputForm(h.get(j).getName(), h.get(j + 1).getName(), h.get(j).getSeqID(), h.get(j + 1).getSeqID(), distance));
                        }
                    }
                }
        }
        return distances = new GetFilesElement(list1, list2);
    }
    public Set<OutputForm> compareDistancesOfAllAtomCoupleOfPdbFileAndStrOrMrFile(List<NMRDistance> NMR, Set<OutputForm> OPF) {
        Set<OutputForm> element = new LinkedHashSet<>();
        HashMap<String, Double> distanceMap = new HashMap<>();
        for (NMRDistance nmr : NMR) {
            for (OutputForm opf : OPF) {
                if ((nmr.atomName1.equals(opf.getAtomName1()) && nmr.atomName2.equals(opf.getAtomName2()))
                        && (nmr.seqID1 == opf.getSeqID1() && nmr.seqID2 == opf.getSeqID2())) {
                    if (opf.getDistanceVal() > nmr.minDistance && opf.getDistanceVal() < nmr.maxDistance) {
                        String key = opf.getAtomName1()+"\n"+opf.getAtomName2()+"\n"+opf.getSeqID1()+"\n"+opf.getSeqID2();
                        if (!distanceMap.containsKey(key)) {
                            distanceMap.put(key, opf.getDistanceVal());
                            element.add(new OutputForm(opf.getAtomName1(), opf.getAtomName2(), opf.getSeqID1(), opf.getSeqID2(), opf.getDistanceVal()));
                        }
                    }
                }
            }
        }

        return element;
    }
    public  double getErrorWithLower(Set<CommonElement> cm, Set<OutputForm> set){
        Iterator<CommonElement> it1 = cm.iterator();
        Iterator<OutputForm> it2 = set.iterator();
        double e = 0.0;
        int n = 0;
        while(it1.hasNext() && it2.hasNext()) {
            CommonElement cm1 = it1.next();
            OutputForm of = it2.next();
            e += of.getDistanceVal() - cm1.getDistanceMin();
            ++n;
        }
        return (double) e / n;
    }
    public  double getErrorWithUpper(Set<CommonElement> elements, Set<OutputForm> set){
        Iterator<CommonElement> it1 = elements.iterator();
        Iterator<OutputForm> it2 = set.iterator();
        double e = 0.0;
        int n = 0;
        while(it1.hasNext() && it2.hasNext()) {
            CommonElement cm = it1.next();
            OutputForm of = it2.next();
            e += cm.getDistanceMax() - of.getDistanceVal();
            ++n;
        }
        return (double) e / n;
    }
    public  double calculateEuclideanDistance(double x1, double x2, double y1, double y2, double z1, double z2){
        return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2) + Math.pow(z1 - z2, 2));
    }
}
