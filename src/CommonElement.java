public class CommonElement {
    private String atomName1, atomName2;
    private int seqID1, seqID2;
    private double distanceMin, distanceMax;

    public CommonElement(String atomName1, String atomName2, int seqID1, int seqID2, double distanceMin, double distanceMax) {
        this.atomName1 = atomName1;
        this.atomName2 = atomName2;
        this.seqID1 = seqID1;
        this.seqID2 = seqID2;
        this.distanceMin = distanceMin;
        this.distanceMax = distanceMax;
    }

    public String getAtomName1() {
        return atomName1;
    }

    public void setAtomName1(String atomName1) {
        this.atomName1 = atomName1;
    }

    public String getAtomName2() {
        return atomName2;
    }

    public void setAtomName2(String atomName2) {
        this.atomName2 = atomName2;
    }

    public int getSeqID1() {
        return seqID1;
    }

    public void setSeqID1(int seqID1) {
        this.seqID1 = seqID1;
    }

    public int getSeqID2() {
        return seqID2;
    }

    public void setSeqID2(int seqID2) {
        this.seqID2 = seqID2;
    }

    public double getDistanceMin() {
        return distanceMin;
    }

    public void setDistanceMin(double distanceMin) {
        this.distanceMin = distanceMin;
    }

    public double getDistanceMax() {
        return distanceMax;
    }

    public void setDistanceMax(double distanceMax) {
        this.distanceMax = distanceMax;
    }
}
