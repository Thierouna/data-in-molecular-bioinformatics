
/* javaDGP project
 *
 * NMR class
 *
 * TODO: This Simon's implementation which uses no other classes of the project... reimplement!
 *
 * last update: November 4th, 2023
 *
 * AM
 */

import java.io.File;
import java.util.*;

class NMRDistance{
    public String atomName1;
    public String atomName2;
    public int seqID1;
    public int seqID2;
    public double minDistance;
    public double maxDistance;
    public double distanceVal;

    public boolean equals(NMRDistance other){
        //check atomNames
        boolean uncertainAtomName = false;
        String[] uncertainty = new String[]{"#", "HN", "M", "Q"};
        for(String tag : uncertainty){
            if(atomName1.contains(tag) || atomName2.contains(tag) || other.atomName1.contains(tag) || other.atomName2.contains(tag))
            {
                uncertainAtomName = true;
                break;
            }
        }

        //If we have an uncertain atom name it is impossible to find a matching pair...
        if(!uncertainAtomName) {
            if(!atomName1.equals(other.atomName1) || !atomName2.equals(other.atomName2)){
                //maybe they are switched up?
                if(!atomName1.equals(other.atomName2) && !atomName2.equals(other.atomName1))
                    return false;
                else {
                    if (seqID1 != other.seqID2 || seqID2 != other.seqID1) return false;
                }
            }
            else
            {
                if(seqID1 != other.seqID1 || seqID2 != other.seqID2)
                {
                    //it could be that atomName1 == atomName2
                    if(atomName1.equals(atomName2)){
                        if (seqID1 != other.seqID2 || seqID2 != other.seqID1) return false;
                    }
                    else
                        return false;
                }
            }
        }

        double epsilon = 0.0001; //make sure to use an epsilon for floating imprecision

        if (Math.abs(this.minDistance - other.minDistance) > epsilon)  return false;
        if (Math.abs(this.maxDistance - other.maxDistance) > epsilon)  return false;
        if (Math.abs(this.distanceVal - other.distanceVal) > epsilon)  return false;
        return true;
    }
}

class NMRAngle{
    public String atomName1;
    public String atomName2;
    public String atomName3;
    public String atomName4;
    public int seqID1;
    public int seqID2;
    public int seqID3;
    public int seqID4;
    public double minAngle;
    public double maxAngle;

    public boolean equals(NMRAngle other){
        if(!atomName1.equals(other.atomName1)) return false;
        if(!atomName2.equals(other.atomName2)) return false;
        if(!atomName3.equals(other.atomName3)) return false;
        if(!atomName4.equals(other.atomName4)) return false;
        if(seqID1 != other.seqID1) return false;
        if(seqID2 != other.seqID2) return false;
        if(seqID3 != other.seqID3) return false;
        if(seqID4 != other.seqID4) return false;

        double epsilon = 0.0001;
        //make sure to use an epsilon for floating imprecision
        if(Math.abs(minAngle - other.minAngle) > epsilon ) return false;
        if(Math.abs(maxAngle - other.maxAngle) > epsilon ) return false;
        return true;
    }
}


/**
 * Class that will read files that hold NMR data.
 *
 */
public class NMR {
    /**
     * The chain of amino acids, as read from the NMR file.
     */
    private List<String> residueChain;
    private final List<NMRDistance> NMRdistances;

    public List<String> getResidueChain() {
        return residueChain;
    }

    public List<NMRDistance> getNMRdistances() {
        return NMRdistances;
    }

    public List<NMRAngle> getNMRangles() {
        return NMRangles;
    }

    private List<NMRAngle> NMRangles;

    /**
     *  A STAR file contains different "saves"\
     *  For every save, we are given a list of identifiers which map to a table, such as torsion angles or distance constraints
     *  Such a table then contains an array of maps, with string identifiers. If there are n observed distances, the length of this array is n
     */


    public void setResidueChain(List<String> residueChain) {
        this.residueChain = residueChain;
    }

    /**
     * Constructs the NMR object by reading from an NMR file (STAR or .mr)
     * @param filename      the NMR file location
     */
    public NMR(String filename) {
        String fileExt = filename.substring(filename.lastIndexOf(".") + 1);
        boolean isStarFile = fileExt.equals("str");

        NMRdistances = new ArrayList<>();
        NMRangles = new ArrayList<>();
        residueChain = new ArrayList<>();

        try {
            File input = new File(filename);
            Scanner scan = new Scanner(input);

            if (isStarFile) {
                ArrayList<HashMap<String, String>> chainEntries = new ArrayList<>();
                ArrayList<HashMap<String, String>> distanceEntries = new ArrayList<>();
                ArrayList<HashMap<String, String>> angleEntries = new ArrayList<>();
                boolean loopMode = false;
                boolean tableFound = false;
                ArrayList<HashMap<String, String>> currentTable = distanceEntries;
                List<String> identifiers = new ArrayList<>();

                while (scan.hasNext()){
                    //split on multiple whitespaces
                    String line = scan.nextLine().trim();
                    if (line.isEmpty())
                        continue;
                    String[] parts = line.split("\\s+");

                    if (loopMode) {
                        if (parts.length == 1) {
                            //We are adding identifiers
                            if (!identifiers.contains(parts[0]))
                                identifiers.add(parts[0]);
                        } else {
                            //We only care about torsion angles, distances and the amino acid chain
                            if (!Set.of(
                                    "_Entity_comp_index.ID",
                                    "_Gen_dist_constraint.ID",
                                    "_Torsion_angle_constraint.ID"
                            ).contains(identifiers.get(0))) {
                                loopMode = false;
                                continue;
                            }

                            //We are adding an entry
                            HashMap<String, String> entry = new HashMap<>();
                            for (int j = 0; j < parts.length; j++)
                                entry.put(identifiers.get(j), parts[j]);
                            currentTable.add(entry);
                        }
                    }

                    if (parts[0].equals("_Entity.Sf_category")) {
                        currentTable = chainEntries;
                        tableFound = true;
                    } else if (parts[0].contains("distance_constraints")) {
                        currentTable = distanceEntries;
                        tableFound = true;
                    } else if (parts[0].contains("dihedral")) {
                        currentTable = angleEntries;
                        tableFound = true;
                    } else if (parts[0].equals("loop_")) {
                        if (tableFound) {
                            loopMode = true;
                            identifiers.clear();
                        }
                    } else if (parts[0].equals("stop_"))
                        loopMode = false;
                }
                //Get the Distance info from the hashtables
                for (HashMap<String, String> entry : distanceEntries) {
                    NMRDistance dist = new NMRDistance();
                    dist.atomName1 = entry.get("_Gen_dist_constraint.Atom_ID_1");
                    dist.atomName2 = entry.get("_Gen_dist_constraint.Atom_ID_2");
                    dist.seqID1 = Integer.parseInt(entry.get("_Gen_dist_constraint.Seq_ID_1"));
                    dist.seqID2 = Integer.parseInt(entry.get("_Gen_dist_constraint.Seq_ID_2"));
                    dist.minDistance = Double.parseDouble(entry.get("_Gen_dist_constraint.Distance_lower_bound_val"));
                    dist.maxDistance = Double.parseDouble(entry.get("_Gen_dist_constraint.Distance_upper_bound_val"));
                    dist.distanceVal = Double.parseDouble(entry.get("_Gen_dist_constraint.Distance_val"));

                    NMRdistances.add(dist);
                }

                //Get the Angle  info from the hashtables
                for (HashMap<String, String> entry : angleEntries) {
                    NMRAngle angle = new NMRAngle();
                    angle.atomName1 = entry.get("_Torsion_angle_constraint.Atom_ID_1");
                    angle.atomName2 = entry.get("_Torsion_angle_constraint.Atom_ID_2");
                    angle.atomName3 = entry.get("_Torsion_angle_constraint.Atom_ID_3");
                    angle.atomName4 = entry.get("_Torsion_angle_constraint.Atom_ID_4");
                    angle.seqID1 = Integer.parseInt(entry.get("_Torsion_angle_constraint.Seq_ID_1"));
                    angle.seqID2 = Integer.parseInt(entry.get("_Torsion_angle_constraint.Seq_ID_2"));
                    angle.seqID3 = Integer.parseInt(entry.get("_Torsion_angle_constraint.Seq_ID_3"));
                    angle.seqID4 = Integer.parseInt(entry.get("_Torsion_angle_constraint.Seq_ID_4"));
                    angle.minAngle = Double.parseDouble(entry.get("_Torsion_angle_constraint.Angle_lower_bound_val"));
                    angle.maxAngle = Double.parseDouble(entry.get("_Torsion_angle_constraint.Angle_upper_bound_val"));
                    NMRangles.add(angle);
                }

                //Get the amino acid chain from the read file
                for (HashMap<String, String> chainEntry : chainEntries) {
                    String letterCode = chainEntry.get("_Entity_comp_index.Comp_ID"); //the three letter residue code
                    residueChain.add(letterCode);
                }
            } else if (fileExt.equals("res")) {
                while (scan.hasNext()){
                    //split on multiple whitespaces
                    String line = scan.nextLine().trim();
                    if (line.isEmpty())
                        continue;

                    line = line.replaceAll("(\\(|\\))", ""); //remove parenthesis

                    String[] parts = line.split("\\s+");

                    if (parts.length == 25) {
                        NMRAngle currentAngle = new NMRAngle();

                        currentAngle.atomName1 = parts[5];
                        currentAngle.atomName2 = parts[10];
                        currentAngle.atomName3 = parts[15];
                        currentAngle.atomName4 = parts[20];

                        currentAngle.seqID1 = Integer.parseInt(parts[2]);
                        currentAngle.seqID2 = Integer.parseInt(parts[7]);
                        currentAngle.seqID3 = Integer.parseInt(parts[12]);
                        currentAngle.seqID4 = Integer.parseInt(parts[17]);

                        double target = Double.parseDouble(parts[22]);
                        double delta = Double.parseDouble(parts[23]);
                        currentAngle.minAngle = target - delta;
                        currentAngle.maxAngle = target + delta;


                        NMRangles.add(currentAngle);
                    }


                }
            } else {
                //Parse MR file
                boolean doingAngleRestraint = false;
                NMRAngle currentAngle = new NMRAngle();
                while (scan.hasNext()){
                    //split on multiple whitespaces
                    String line = scan.nextLine().trim();
                    line = line.replaceAll("(\\(|\\))", ""); //remove parenthesis
                    if (line.isEmpty())
                        continue;
                    String[] parts = line.split("\\s+");

                    //We are doing a restraint
                    if (parts[0].equals("assign")) {
                        if (parts.length == 11) {
                            //torsion angle restraint
                            doingAngleRestraint = true;
                            currentAngle = new NMRAngle();
                            currentAngle.seqID1 = Integer.parseInt(parts[2]);
                            currentAngle.atomName1 = parts[5];
                            currentAngle.seqID2 = Integer.parseInt(parts[7]);
                            currentAngle.atomName2 = parts[10];
                        }
                        if (parts.length == 14 || parts.length == 16) {
                            //distance restraint
                            NMRDistance dist = new NMRDistance();
                            dist.seqID1 = Integer.parseInt(parts[2]);
                            dist.atomName1 = parts[5];
                            dist.seqID2 = Integer.parseInt(parts[7]);
                            dist.atomName2 = parts[10];

                            dist.distanceVal = Double.parseDouble(parts[11]);
                            double minus = Double.parseDouble(parts[12]);
                            double plus = Double.parseDouble(parts[13]);
                            dist.minDistance = dist.distanceVal - minus;
                            dist.maxDistance = dist.distanceVal + plus;
                            NMRdistances.add(dist);
                        }
                    } else if (doingAngleRestraint) {
                        //second half of angle restraint
                        currentAngle.seqID3 = Integer.parseInt(parts[1]);
                        currentAngle.atomName3 = parts[4];
                        currentAngle.seqID4 = Integer.parseInt(parts[6]);
                        currentAngle.atomName4 = parts[9];

                        double angleMiddle = Double.parseDouble(parts[11]);
                        double angleDev = Double.parseDouble(parts[12]);

                        currentAngle.minAngle = angleMiddle - angleDev;
                        currentAngle.maxAngle = angleMiddle + angleDev;
                        NMRangles.add(currentAngle);
                        doingAngleRestraint = false;
                    }
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
            System.exit(1);
        }
    }

    public boolean contains(NMR other){
        //MR files do not contain residue chains.
        List<NMRAngle> otherAngles = other.getNMRangles();
        List<NMRDistance> otherDistances = other.getNMRdistances();

        for(NMRAngle otherAngle : otherAngles){
            boolean foundPair = false;
            for(NMRAngle ourAngle : this.getNMRangles())
            {
                if(ourAngle.equals(otherAngle))
                {
                    foundPair = true;
                    break;
                }
            }
            if(!foundPair) {
                System.out.println("Cannot find angle: " + otherAngle.atomName1 + " " + otherAngle.seqID1 + " " + otherAngle.atomName2 + " " + otherAngle.seqID2 + " " +  otherAngle.atomName3 + " " + otherAngle.seqID3);

                return false;
            }
        }
        for(NMRDistance otherDist : otherDistances){
            boolean foundPair = false;
            for(NMRDistance ourDist : this.getNMRdistances())
            {
                if(ourDist.equals(otherDist))
                {
                    foundPair = true;
                    break;
                }
            }
            if(!foundPair) {
                System.out.println("Cannot find distance: " + otherDist.seqID1 + " " + otherDist.seqID2 + " " + otherDist.atomName1 + " " + otherDist.atomName2 + " " + otherDist.minDistance + " " + otherDist.maxDistance);

                return false;
            }
        }
        return true;
    }

    // main
    public static void main(String[] args)
    {
        System.out.print("NMR class ... ");
        try {
            NMR nmrSTR = new NMR("2jmy_mr.str");
            NMR nmr = new NMR("2jmy.mr");

            if(nmrSTR.getResidueChain().isEmpty() || nmrSTR.getNMRdistances().isEmpty() || nmrSTR.getNMRangles().isEmpty())
                throw new Exception("Filling the required data structures failed! (2jmy_mr.str)");
            if(nmr.getNMRdistances().isEmpty() || nmr.getNMRangles().isEmpty())
                throw new Exception("Filling the required data structures failed! (2jmy.mr)");

            if(!nmrSTR.contains(nmr))
                throw new Exception("Reading from .mr and .str files gives two different results!");

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        System.out.println("OK");
    }
}


